$(document).ready(function(){
    $('.btn_scroll').on('click', function(e){
        e.preventDefault();
        var t = 1000;
        var d = $(this).attr('data-href') ? $(this).attr('data-href') : $(this).attr('href');
        $('html,body').stop().animate({ scrollTop: $(d).offset().top - 50 }, t);
    });
});


(function() {

    $('.im-faq-title').on('click touchstart', function(e){
        e.preventDefault();

        $(this).closest('li').toggleClass('open');
    });

}());


$('.im-boat-slider').owlCarousel({
    loop:true,
    margin:0,
    items:1,
    nav:false,
    autoplay:true,
    autoplayTimeout:4000,
    autoplayHoverPause:true
});


var myMap;

ymaps.ready(function () {
    myMap = new ymaps.Map('map', {
        zoom: 15,
        center: [55.75626664952321,37.439763526489266],
        controls: ['smallMapDefaultSet']
    }, {
        searchControlProvider: 'yandex#search',
    });
    myMap.geoObjects
        .add(new ymaps.Placemark([55.75457256900345,37.43521450000001], {
            balloonContent: 'Крылатская улица 2к4',
            iconCaption: 'Крылатская улица 2к4'
        }, {
            preset: 'islands#blueCircleDotIconWithCaption',
            iconCaptionMaxWidth: '200'
        }))
        .add(new ymaps.Placemark([55.75610256900737,37.444098499999974], {
            balloonContent: 'Крылатская улица 2',
            iconCaption: 'Крылатская улица 2'
        }, {
            preset: 'islands#blueCircleDotIconWithCaption',
            iconCaptionMaxWidth: '200'
        }));

    myMap.behaviors
    // Отключаем часть включенных по умолчанию поведений:
    //  - drag - перемещение карты при нажатой левой кнопки мыши;
    //  - magnifier.rightButton - увеличение области, выделенной правой кнопкой мыши.
        .disable(['drag', 'rightMouseButtonMagnifier'])
        // Включаем линейку.
        .enable('ruler');

    // Изменяем свойство поведения с помощью опции:
    // изменение масштаба колесом прокрутки будет происходить медленно,
    // на 1/2 уровня масштабирования в секунду.
    myMap.options.set('scrollZoomSpeed', 1);
});


